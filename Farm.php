<?php

abstract class Entity { // Существа
}

interface getAnimalInterface {
    public function getNameAnimal(): String; //получить название животного
}

abstract class Animal extends Entity implements getAnimalInterface { // Животное
    private $id; // номер животного
    private $nameAnimal; // имя животного

    public function __construct(String $nameAnimal) {
        $this->id = md5(rand());
        $this->nameAnimal = $nameAnimal;
    }

    public function getNameAnimal(): string
    {
        return $this->nameAnimal;
    }

    public abstract function getQtyProduct(): int;
    public abstract function getNameProduct(): string;

}

interface getAnimalEventsInterface {
    public function getNameProduct(): String; //получить названите продукта (Яйцо / молоко / мясо и т.д.)
    public function getQtyProduct(): int; //получить кол-во продукта
}

class Cow extends Animal implements getAnimalEventsInterface{
    private $nameProduct; // имя продукта
    private $qtyProduct; // производит кол-во молока за один раз

    public function __construct(string $nameAnimal, string $nameProduct, int $qtyProduct)
    {
        parent::__construct($nameAnimal);
        $this->nameProduct = $nameProduct;
        $this->qtyProduct = $qtyProduct;
    }

    public function getNameProduct(): string
    {
        return $this->nameProduct;
    }

    public function getQtyProduct(): int
    {
        return $this->qtyProduct;
    }
}

class Chicken extends Animal implements getAnimalEventsInterface {
    private $nameProduct; // имя продукта
    private $qtyProduct; // кол-во продукта за один раз

    public function __construct(string $nameAnimal, string $nameProduct, int $qtyProduct)
    {
        parent::__construct($nameAnimal);
        $this->nameProduct = $nameProduct;
        $this->qtyProduct = $qtyProduct;
    }

    public function getNameProduct(): string
    {
        return $this->nameProduct;
    }

    public function getQtyProduct(): int
    {
        return $this->qtyProduct;
    }
}

abstract class Building {
    private $nameBuilding; // название строения
    private $typeBuilding; // тип строения
    private $numberFloors; // нормер строения

    public function __construct(String $nameBuilding, String $typeBuilding, int $numberFloors) {
        $this->nameBuilding = $nameBuilding;
        $this->typeBuilding = $typeBuilding;
        $this->numberFloors = $numberFloors;
    }
}

class Farm extends Building
{
    private $animals = []; //животные
    private $commodityResearch = []; // отдел товаровед, считает кол-во продукции
//    private $commodityResearchAnimal = []; // отдел товаровед, записывает кол-во животных

    public function addAnimal(Animal $animal) { //добавили животного
        $this->animals[] = $animal; // добавили животное в массив
        $this->setCommodityResearch($animal->getNameProduct(), $animal->getQtyProduct(), $animal->getNameAnimal()); // передаем отделу товаровед (считает кол-во продукции и записывать)
    }

    public function setCommodityResearch(String $nameProduct, int $qtyProduct, String $nameAnimal) { //отдел товаровед (считает кол-во продукции, животных и записывает их)
        if (array_key_exists($nameAnimal, $this->commodityResearch)) {
            $qty = $this->commodityResearch[$nameAnimal][$nameAnimal];
            $product = $this->commodityResearch[$nameAnimal][$nameProduct];
            $this->commodityResearch[$nameAnimal] = [$nameAnimal => $qty + 1, $nameProduct => $product + $qtyProduct];
        } else {
            $this->commodityResearch[$nameAnimal] = [$nameAnimal => 1, $nameProduct => $qtyProduct];
        }
    }

    public function getCommodityResearch(String $animal) { // отдел товаровед передает кол-во продукции
        foreach ($this->commodityResearch[$animal] as $key => $value) {
            echo $key." = ".$value." ";
        }
        echo "\n---------------------\n";
    }

    public function getCommodityResearchAll() { // отдел товаров передает полный отчет продукции
        foreach ($this->commodityResearch as $animalAll) {
            foreach ($animalAll as $key => $value) {
                echo $key." = ".$value." ";
            }
            echo "\n";
        }
        echo "--------------------------\n";
    }
}

$farm = new Farm("FastFarm", "Farm", 7); // создали ферму (название, тип строения, номер строения)

for ($i = 0; $i < 10; $i++) { // добавляем живтных
    $farm->addAnimal(new Cow("Корова", "Молоко", rand(8, 12)));
}

$farm->getCommodityResearch("Корова"); // вывод отчет по одного животного

for ($i = 0; $i < 20; $i++) {
    $farm->addAnimal(new Chicken("Курица", "Яйца", rand(0, 1)));
}

$farm->getCommodityResearchAll(); // выдает отчет по всем животнам и продукции